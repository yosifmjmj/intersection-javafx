package com.mycompany.intersection;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Holds compute button handler, validation logic and execution timing
 *
 * @author stefanov
 */
public class FXMLController implements Initializable {

    private static final Logger logger = LogManager.getLogger(FXMLController.class);

    @FXML
    private TextField inputList1;

    @FXML
    private TextField inputList2;

    @FXML
    private TextArea resultArea;

    @FXML
    private void handleComputeAction(ActionEvent event) {
        Set<Integer> res;
        logger.debug("Compute handler entered");
        Long start = System.currentTimeMillis();

        Boolean isValid = validateInput(inputList1, inputList2);
        if (isValid) {
            logger.debug("Input is valid.");
            res = IntersectUtils.calculate(inputList1.getText().trim(), inputList2.getText().trim());
            resultArea.setText(res.toString());

            Long end = System.currentTimeMillis();
            logger.info("Execution finished in {} millis", end - start);

        } else {
            logger.debug("Input is invalid!");
            Alert alert = new Alert(AlertType.WARNING,
                    "Lists are empty or contain nin-numeric symbols!", ButtonType.OK);
            alert.showAndWait();
        }

    }

    private Boolean validateInput(TextField inputList1, TextField inputList2) {
        Boolean res = Boolean.FALSE;
        logger.debug("Validating input");

        if (inputList1.getText() != null && inputList1.getText() != null) {
            String inputTxt1 = inputList1.getText().trim();
            String inputTxt2 = inputList2.getText().trim();

            if (!inputTxt1.isEmpty() && !inputTxt2.isEmpty()) {
                Pattern p = Pattern.compile("[0-9, ]+");

                if (p.matcher(inputTxt1).matches() && p.matcher(inputTxt2).matches()) {
                    res = Boolean.TRUE;
                }
            }
        }

        return res;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
