package com.mycompany.intersection;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Contains main logic for manipulation of the input and intersection
 *
 * @author stefanov
 */
class IntersectUtils {

    private static final Logger logger = LogManager.getLogger(IntersectUtils.class);

    public static Set<Integer> calculate(final String input_1, final String input_2) {
        Set<Integer> res = new HashSet<>();
        logger.info("Calculating input values");

        Set<Integer> set_1 = split(input_1);
        Set<Integer> set_2 = split(input_2);

        if (set_1.size() > set_2.size()) {
            res = intersect(set_2, set_1);
        } else {
            res = intersect(set_1, set_2);
        }

        logger.debug("Calculation finished");
        return res;
    }

    private static Set<Integer> split(final String input) {
        Set<Integer> res = new HashSet<>();
        logger.debug("Splitting list to integers.");

        String[] splitted = input.split(",\\s|,|\\s");
        for (String s : splitted) {
            s = s.trim();
            if (s.length() > 0) {
                res.add(Integer.valueOf(s));
            }
        }

        return res;
    }

    private static Set<Integer> intersect(Set<Integer> smaller, Set<Integer> bigger) {
        Set<Integer> res;
        logger.info("Intersecting the two sets");

        res = smaller.stream().filter(num -> bigger.contains(num)).collect(Collectors.toSet());

        logger.info("Done intersecting");
        return res;
    }

}
