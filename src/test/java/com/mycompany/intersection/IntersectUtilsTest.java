package com.mycompany.intersection;

import java.util.Set;
import org.junit.Assert;
import org.junit.Test;

/**
 * Simple tests to validate the core calculate intersection functionality
 *
 * @author stefanov
 */
public class IntersectUtilsTest {

    @Test
    public void calculateEmpty() {
        String input_1 = "";
        String input_2 = "";
        Set<Integer> res = IntersectUtils.calculate(input_1, input_2);

        Assert.assertTrue(res != null);
        Assert.assertTrue(res.isEmpty());

        // --
        input_1 = null;
        input_2 = null;

        Boolean isThrown = Boolean.FALSE;
        try {
            IntersectUtils.calculate(input_1, input_2);
        } catch (Exception exc) {
            isThrown = Boolean.TRUE;
        }

        Assert.assertTrue(isThrown);

    }

    @Test
    public void calculateFullLists() {
        String input_1 = "1, 2, 3, 4, 5";
        String input_2 = "4, 5";
        Set<Integer> res = IntersectUtils.calculate(input_1, input_2);

        Assert.assertTrue(res != null);
        Assert.assertTrue(res.toString().equals("[4, 5]"));

        //--
        input_1 = ", 4, 5,   3    ";
        input_2 = "6 24 3 3 3 4 4 77 8 123";
        res = IntersectUtils.calculate(input_1, input_2);

        Assert.assertTrue(res != null);
        Assert.assertTrue(res.toString().equals("[3, 4]"));

        //--
        input_1 = ",1,22,5677";
        input_2 = "22 09 21  5677   34344 ";
        res = IntersectUtils.calculate(input_1, input_2);

        Assert.assertTrue(res != null);
        Assert.assertTrue(res.toString().equals("[22, 5677]"));
    }

}
